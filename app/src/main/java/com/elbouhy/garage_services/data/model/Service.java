package com.elbouhy.garage_services.data.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Service {

    private int mServiceID;
    private int mImageID;
    private String mTitle;
    private String mSubTitle;


    public Service(int mImageID) {
        this.mImageID = mImageID;
    }

    public Service(String mTitle,int mImageID) {
        this.mImageID = mImageID;
    }


    public Service(int mServiceID, int mImageID, String mTitle, String mSubTitle) {
        this.mServiceID = mServiceID;
        this.mImageID = mImageID;
        this.mTitle = mTitle;
        this.mSubTitle = mSubTitle;
    }

    public int getmServiceID() {
        return mServiceID;
    }

    public void setmServiceID(int mServiceID) {
        this.mServiceID = mServiceID;
    }

    public int getmImageID() {
        return mImageID;
    }

    public void setmImageID(int mImageID) {
        this.mImageID = mImageID;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmSubTitle() {
        return mSubTitle;
    }

    public void setmSubTitle(String mSubTitle) {
        this.mSubTitle = mSubTitle;
    }
}
