package com.elbouhy.garage_services.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.elbouhy.garage_services.R;
import com.elbouhy.garage_services.data.model.Service;
import com.elbouhy.garage_services.ui.adaptor.ServiceAdaptor;
import com.elbouhy.garage_services.ui.listener.OnItemSelectListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnItemSelectListener {

    private RecyclerView mRecyclerView;
    private ServiceAdaptor mServiceAdaptor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        getData();

    }

    public void getData(){

        ArrayList<Service> mservicesList = new ArrayList<>();

        mservicesList.add(new Service("Services",R.drawable.our_services));
        mservicesList.add(new Service("Our Work",R.drawable.our_work));
        mservicesList.add(new Service("About Us",R.drawable.about));
        mservicesList.add(new Service("Find Us",R.drawable.findus));
        mservicesList.add(new Service("Book Now",R.drawable.book_now));


        mRecyclerView = findViewById(R.id.rv_main);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mServiceAdaptor = new ServiceAdaptor(this,mservicesList,this);
        mRecyclerView.setAdapter(mServiceAdaptor);

    }

    @Override
    public void OnItemClicked(Service service) {

        int mImageID = service.getmImageID();
        Intent intent;

        switch(mImageID){
            case R.drawable.our_services :

       startActivity (new Intent(MainActivity.this,ServicesActivity.class));

             break;
           case R.drawable.our_work :

               startActivity (new Intent(MainActivity.this,OurWorkActivity.class));

                break;
            case R.drawable.about :
                startActivity (new Intent(MainActivity.this,AboutUsActivity.class));

                break;
            case R.drawable.findus :
                startActivity (new Intent(MainActivity.this,FindUsActivity.class));

                break;
            case R.drawable.book_now :
                startActivity (new Intent(MainActivity.this,BookingActivity.class));

                break;
        }





    }

    public void ShowBookingActivity(View view) {
        startActivity (new Intent(this,BookingActivity.class));

    }
}
