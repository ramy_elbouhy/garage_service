package com.elbouhy.garage_services.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.elbouhy.garage_services.R;

public class OurWorkActivity extends AppCompatActivity {

    private ImageView mImage1;
    private ImageView mImage2;
    private ImageView mImage3;
    private ImageView mImage4;
    private ImageView mImage5;
    private ImageView mImage6;
    private ImageView mImage7;
    private ImageView mImage8;
    private ImageView mImage9;
    private ImageView mImage10;
    private ImageView mImage11;
    private ImageView mImage12;
    private ImageView mImage13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_our_work);

        getSupportActionBar().setTitle(" Our Work");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mImage1 = findViewById(R.id.img1);
        mImage2 = findViewById(R.id.img2);
        mImage3 = findViewById(R.id.img3);
        mImage4 = findViewById(R.id.img4);
        mImage5 = findViewById(R.id.img5);
        mImage6 = findViewById(R.id.img6);
        mImage7 = findViewById(R.id.img7);
        mImage8 = findViewById(R.id.img8);
        mImage9 = findViewById(R.id.img9);
        mImage10 = findViewById(R.id.img10);
        mImage11 = findViewById(R.id.img11);
        mImage12 = findViewById(R.id.img12);
        mImage13 = findViewById(R.id.img13);





        Glide.with(this)
                .asBitmap()
                .load(R.drawable.ourwork_1).into(mImage1);

        Glide.with(this)
                .asBitmap()
                .load(R.drawable.ourwork_2).into(mImage2);


        Glide.with(this)
                .asBitmap()
                .load(R.drawable.ourwork_3).into(mImage3);

        Glide.with(this)
                .asBitmap()
                .load(R.drawable.ourwork_4).into(mImage4);

        Glide.with(this)
                .asBitmap()
                .load(R.drawable.ourwork_5).into(mImage5);


        Glide.with(this)
                .asBitmap()
                .load(R.drawable.ourwork_6).into(mImage6);



        Glide.with(this)
                .asBitmap()
                .load(R.drawable.ourwork7).into(mImage7);

        Glide.with(this)
                .asBitmap()
                .load(R.drawable.ourwork8).into(mImage8);


        Glide.with(this)
                .asBitmap()
                .load(R.drawable.ourwork9).into(mImage9);

        Glide.with(this)
                .asBitmap()
                .load(R.drawable.ourwork12).into(mImage12);

        Glide.with(this)
                .asBitmap()
                .load(R.drawable.ourwork10).into(mImage10);


        Glide.with(this)
                .asBitmap()
                .load(R.drawable.ourwork11).into(mImage11);

        Glide.with(this)
                .asBitmap()
                .load(R.drawable.ourwork7).into(mImage13);



    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void ShowBookingActivity(View view) {
        startActivity (new Intent(this,BookingActivity.class));
    }
}
