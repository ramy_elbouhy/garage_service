package com.elbouhy.garage_services.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.elbouhy.garage_services.R;
import com.elbouhy.garage_services.data.model.Service;
import com.elbouhy.garage_services.ui.adaptor.ServiceAdaptor;
import com.elbouhy.garage_services.ui.listener.OnItemSelectListener;

import java.util.ArrayList;

public class ServicesActivity extends AppCompatActivity implements OnItemSelectListener {

    private RecyclerView mRecyclerView;
    private ServiceAdaptor mServiceAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);


        getSupportActionBar().setTitle(" Our Services");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

      getServicesData();

    }
    public void getServicesData(){

        ArrayList<Service> servicesList = new ArrayList<>();

        servicesList.add(new Service(R.drawable.card_wash));
        servicesList.add(new Service(R.drawable.card_maintain));
        servicesList.add(new Service(R.drawable.card_repair));



        mRecyclerView = findViewById(R.id.rv_services);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mServiceAdaptor = new ServiceAdaptor(this,servicesList,this);
        mRecyclerView.setAdapter(mServiceAdaptor);

    }


    @Override
    public void OnItemClicked(Service service) {

        startActivity (new Intent(this,BookingActivity.class));


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void ShowBookingActivity(View view) {
        startActivity (new Intent(this,BookingActivity.class));
    }
}
