package com.elbouhy.garage_services.ui.adaptor;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.elbouhy.garage_services.R;
import com.elbouhy.garage_services.data.model.Service;
import com.elbouhy.garage_services.ui.listener.OnItemSelectListener;

import java.util.List;

public class ServiceAdaptor extends RecyclerView.Adapter<ServiceAdaptor.ServiceViewHolder> {

    private Context mContext;
    private List<Service> mServices;
    private OnItemSelectListener onItemSelectListener;

    public ServiceAdaptor(Context mContext, List<Service> mServices, OnItemSelectListener onItemSelectListener) {
        this.mContext = mContext;
        this.mServices = mServices;
        this.onItemSelectListener = onItemSelectListener;
    }

    @NonNull
    @Override
    public ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_service_vertical_layout,viewGroup,false);

        return new ServiceViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ServiceViewHolder serviceViewHolder, final int position) {

        Service service = mServices.get(position);
        int mservice = mServices.get(position).getmImageID();

        Glide.with(mContext)
                .asBitmap()
                .load(service.getmImageID()).into(serviceViewHolder.mImageView);

        serviceViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemSelectListener.OnItemClicked(mServices.get(serviceViewHolder.getAdapterPosition()));
            }
        });



    }

    @Override
    public int getItemCount() {
        return mServices.size();
    }


    public class ServiceViewHolder extends RecyclerView.ViewHolder{

    private ImageView mImageView;


    public ServiceViewHolder(@NonNull View itemView) {
        super(itemView);

        mImageView = itemView.findViewById(R.id.img_card_1);

    }
}

}
