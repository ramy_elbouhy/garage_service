package com.elbouhy.garage_services.ui.listener;

import com.elbouhy.garage_services.data.model.Service;

public interface OnItemSelectListener {

    void OnItemClicked (Service service);
}
